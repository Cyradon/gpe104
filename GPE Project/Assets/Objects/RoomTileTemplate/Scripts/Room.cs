﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour {

	public GameObject doorNorth;
	public GameObject doorSouth;
	public GameObject doorEast;
	public GameObject doorWest;

    public GameObject GuardianBot;
    public GameObject HoundBot;
    public GameObject FoxBot;
    public GameObject PathBot;

    public Transform PlayerEntryPoint;

    public Transform PowerupSpawnOne;
    public Transform PowerupSpawnTwo;

}
