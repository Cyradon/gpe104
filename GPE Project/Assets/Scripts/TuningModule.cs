﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuningModule : MonoBehaviour {

    // Editable Speed value for forward movement
    public float TankSpeedValue = 3;

    // Editable rotational value for Left/Right turning
    public float TankTurnValue = 180;

    // Editable Speed value for backward movement
    public float TankBackingValue = 2;

    // Editable Health value for player and robot alike
    public int HealthCount = 3;

    // used to compare how much damage has been taken since beginning
    public int CurrentHealth;

    // It takes time to load another shell!
    public float Reload = 0.5f;
}
