﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIModule : MonoBehaviour {

    public GameObject PlayerObject;

    public int Playershealth;

    public string HealthDisplay;

    public Text PlayerHealthDisplay;

	// Use this for initialization
	void Start () {
        Invoke("FindPlayerTank", 0.5f);
        

        
	}
	
	// Update is called once per frame
	void Update () {
        HealthDisplay = "Health: " + Playershealth;
        PlayerHealthDisplay.text = HealthDisplay;
        Playershealth = PlayerObject.GetComponent<TuningModule>().CurrentHealth;
    }

    public void FindPlayerTank() {
        PlayerObject = GameObject.FindGameObjectWithTag("Player");
    }
}
