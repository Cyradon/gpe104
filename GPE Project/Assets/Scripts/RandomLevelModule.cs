﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomLevelModule : MonoBehaviour {

    public int BaseRow;

    public int BaseCol;

    private float BaseTileWide = 50.0f;

    private float BaseTileLong = 50.0f;

    public GameObject[] BaseBuildTileList;

    public Transform[,] PotentialStarts = new Transform[5,5];

    public Room[,] BaseGridLayout;

    public bool DailyBaseTarget;

    public bool IsRandomBase;

    public int BaseLayoutSeed;

    public GameObject PlayerTank;

    // Use this for initialization
    void Start () {
        if (EventsModule.IsMapOfDay) {
            BaseLayoutSeed = CurrentTimeFrameToNum(DateTime.Now.Date);
        }

        if (EventsModule.IsRandomLoad) {
            BaseLayoutSeed = CurrentTimeFrameToNum(DateTime.Now);
        }

        CreateBaseLayout();
        
	}

    public int CurrentTimeFrameToNum(DateTime CurrentTime) {
        return CurrentTime.Year + CurrentTime.Month + CurrentTime.Day + CurrentTime.Hour + CurrentTime.Minute + CurrentTime.Second + CurrentTime.Millisecond;
    }

    public void CreateBaseLayout() {
        
        UnityEngine.Random.InitState(BaseLayoutSeed);
        
        BaseGridLayout = new Room[BaseRow, BaseCol];

        for (int RowAssign = 0; RowAssign < BaseRow; RowAssign++) {
            for (int ColAssign = 0; ColAssign < BaseCol; ColAssign++) {
                float XLocale = BaseTileWide * ColAssign;
                float ZLocale = BaseTileLong * RowAssign;

                Vector3 NextLocale = new Vector3(XLocale, 0.0f, ZLocale);

                GameObject HypotheticalBaseTile = Instantiate(BaseRandomTileSelection(), NextLocale, Quaternion.identity) as GameObject;

                HypotheticalBaseTile.transform.parent = this.transform;

                HypotheticalBaseTile.name = "Tile Designation" + ColAssign + "By" + RowAssign;

                Room ScoutedTile = HypotheticalBaseTile.GetComponent<Room>();

                if (RowAssign == 0)
                {
                    ScoutedTile.doorNorth.SetActive(false);
                }
                else if (RowAssign == BaseCol - 1)
                {
                    ScoutedTile.doorSouth.SetActive(false);
                }
                else {
                    ScoutedTile.doorNorth.SetActive(false);
                    ScoutedTile.doorSouth.SetActive(false);
                }

                if (ColAssign == 0)
                {
                    ScoutedTile.doorEast.SetActive(false);
                }
                else if (ColAssign == BaseCol - 1)
                {
                    ScoutedTile.doorWest.SetActive(false);
                }
                else
                {
                    ScoutedTile.doorEast.SetActive(false);
                    ScoutedTile.doorWest.SetActive(false);
                }

                BaseGridLayout[RowAssign, ColAssign] = ScoutedTile;
                PotentialStarts[RowAssign, ColAssign] = BaseGridLayout[RowAssign, ColAssign].PlayerEntryPoint;
            }
        }

        SpawnPlayerTank();
        SpawnEnemyTanks();
    }

    public GameObject BaseRandomTileSelection() {
        return BaseBuildTileList[UnityEngine.Random.Range(0, BaseBuildTileList.Length)];
    }

    public void SpawnPlayerTank() {
        Instantiate(PlayerTank, PotentialStarts[UnityEngine.Random.Range(0, 4), UnityEngine.Random.Range(0, 4)].transform.position, Quaternion.identity);
    }

    public void SpawnEnemyTanks() {
        for (int RoomAssignRow = 0; RoomAssignRow < BaseRow; RoomAssignRow++)
        {
            for (int RoomAssignCol = 0; RoomAssignCol < BaseCol; RoomAssignCol++)
            {
                int SelectTank = UnityEngine.Random.Range(0, 3);
                if (SelectTank == 0)
                {
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].GuardianBot.SetActive(true);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].PathBot.SetActive(false);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].HoundBot.SetActive(false);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].FoxBot.SetActive(false);
                }
                else if (SelectTank == 1)
                {
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].GuardianBot.SetActive(false);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].PathBot.SetActive(true);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].HoundBot.SetActive(false);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].FoxBot.SetActive(false);
                }
                else if (SelectTank == 2)
                {
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].GuardianBot.SetActive(false);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].PathBot.SetActive(false);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].HoundBot.SetActive(true);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].FoxBot.SetActive(false);
                }
                else if (SelectTank == 3)
                {
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].GuardianBot.SetActive(false);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].PathBot.SetActive(false);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].HoundBot.SetActive(false);
                    BaseGridLayout[RoomAssignRow, RoomAssignCol].FoxBot.SetActive(true);
                }
            }
        }
    }

    
}
