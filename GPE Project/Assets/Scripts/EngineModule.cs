﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineModule : MonoBehaviour {

    // Use this to story the control component
    private CharacterController TankCommander;

    // store the transform of the parent object
    public Transform TankPos;

    // This keeps the Shell component
    public Rigidbody TankShell;

    // The empty from which the shell spawns is stored here
    public Transform CannonEnd;

    // How fast the shell moves from cannon to target
    public float FlightSpeed = 1000f;
    
    

    // The AI cannot push a real button so we had to make it a button.
    public bool CanAIFire;

    public bool GoThatWay(Vector3 SectorName, float Swift) {
        // Figure out how far the tank needs to turn to go to the next waypoint
        Quaternion NeededRotate = Quaternion.LookRotation(SectorName - TankPos.position);
        // if it's already looking at it...
        if (NeededRotate == TankPos.rotation)
        {
            // then all is well, and nothing more needs to be done
            return false;
        }

        // the tank turns if it needs to...
        TankPos.rotation = Quaternion.RotateTowards(TankPos.rotation, NeededRotate, Swift * Time.deltaTime);
        // Notifying its controlling personality of the change in direction
        return true;
    }

    public void Awake()
    {
        // Use this for any transform-related stuff
        TankPos = gameObject.GetComponent<Transform>();
    }

    // Use this for initialization
    void Start () {
        
        // Get the attached object's control component and plug it in
        TankCommander = gameObject.GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // the engine moves the tank like this
    public void MoveTank(float moveSpeed) {

        // speed factor to move the tank. Take the input and multiply Forward by parameter.
        Vector3 TankVector = TankPos.forward * moveSpeed;

        // now we add movement to the object
        TankCommander.SimpleMove(TankVector);
    }

    // Use this to turn the tank so it faces the intended fire direction
    public void TurnTank(float TurnRate) {

        // to get rotational factor, multiply the up vector by the given parameter
        // then multiply that result by time in seconds
        Vector3 TankRotation = (Vector3.up * TurnRate) * Time.deltaTime;

        // this is what actually applies the change to the tank
        TankPos.Rotate(TankRotation, Space.Self);
    }

    // Generic Fire function useable by Player and AI alike
    public void FireCannon() {

        // Spawn the shell into the world
        Rigidbody ShotFired = Instantiate(TankShell, CannonEnd.position, Quaternion.identity) as Rigidbody;

        // Give said shell a movement speed
        ShotFired.AddForce(CannonEnd.transform.forward * FlightSpeed);
    }

    
}
