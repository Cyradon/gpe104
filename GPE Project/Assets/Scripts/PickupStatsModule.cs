﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupStatsModule : MonoBehaviour {

    public PickupModule Upgrade;

    public AudioClip ChaChing;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider TankyBoy)
    {
        PowerupModule UpgradeManager = TankyBoy.GetComponent<PowerupModule>();

        if (UpgradeManager != null) {
            UpgradeManager.IncreasePower(Upgrade);

            if (ChaChing != null) {
                AudioSource.PlayClipAtPoint(ChaChing, this.transform.position, 1.0f);
            }
            Destroy(this.gameObject);
        }
    }
}
