﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoringModule : MonoBehaviour {

    // This is the score count for the player
    public static ScoringModule TankScore;

    public int CurrentCount;

    // Use this for initialization
    void Start () {
        TankScore = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
