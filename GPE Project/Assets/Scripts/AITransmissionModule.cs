﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITransmissionModule : MonoBehaviour {

    // Personality Control for the AI Tanks
    public enum AIpersonalityConfig {PathFinder, Bloodhound, Guardian, RedFox};

    // Sets the default personality for the AI Tank in question
    public AIpersonalityConfig PConfig = AIpersonalityConfig.Bloodhound;

    // All tanks use an engine and the AIs are by no means exempt from this
    public EngineModule BotEngine;

    // Even robots have to be fine tuned. that's where this comes in
    public TuningModule BotTuning;

    public RandomLevelModule LevelGenerator;

    // is the cannon ready to fire?
    private float BotCannonReady;

    private Transform TankOrigin;

    public Transform PlayerLocation;

    public float SoundRange = 3f;

    // These are the arrays for all the bots to follow
    public Transform[] MainPath = new Transform[24];
    public Transform[] GuardianPath = new Transform[4];
    public Transform[] HoundFoxPath = new Transform[12];

    private int MyTwenty = 0;

    public float RelativeArea = 0.5f;

    private bool CanHearPlayer;

    private Vector3 RetreatVector;

    void Awake()
    {
        TankOrigin = gameObject.GetComponent<Transform>();
        LevelGenerator = GameObject.FindGameObjectWithTag("Overwatch").GetComponent<RandomLevelModule>();
        
    }

    // Use this for initialization
    void Start () {
        // starting off the timer to reload for the AI
        BotCannonReady = Time.time + BotTuning.Reload;

        // Sets the initial hull integrity of tank
        BotTuning.CurrentHealth = BotTuning.HealthCount;

        CanHearPlayer = false;

        BotEngine.CanAIFire = true;

        PlayerLocation = GameObject.Find("PlayerTank(Clone)").transform;
    }
	
	// Update is called once per frame
	void Update () {
        PersonalityMove();
        GuardsmenRoutine();
        
    }

    void PersonalityMove() {
        
        switch (PConfig) {
            case AIpersonalityConfig.Bloodhound:
                BloodhoundBehavior();
                break;
            case AIpersonalityConfig.Guardian:
                GuardianBehavior();
                break;
            case AIpersonalityConfig.PathFinder:
                PathfinderBehavior();
                break;
            case AIpersonalityConfig.RedFox:
                RedFoxBehavior();
                break;
        }
    }

    public void OnCollisionEnter(Collision TankBody)
    {

        // Check for being hit by a shell at all, be it enemy or player
        if (TankBody.gameObject.tag == "EnemyShell" || TankBody.gameObject.tag == "PlayerShell")
        {
            // reduce health on hit
            BotTuning.CurrentHealth--;

            // If the tank that was attacked was the bloodhound tank
            if (BotTuning.CurrentHealth <= 0 && this.gameObject.tag == "Bloodhound")
            {
                // let the observer know that the bloodhound was destroyed
                BattlefieldOverwatch.Observer.DestroyedEnemyOne = true;
                // get on with actually removing it from the game
                Destroy(this.gameObject);
            }

            // Same deal with guardian as above
            if (BotTuning.CurrentHealth <= 0 && this.gameObject.tag == "Guardian")
            {
                BattlefieldOverwatch.Observer.DestroyedEnemyTwo = true;
                Destroy(this.gameObject);
            }

            // ditto with pathfinder
            if (BotTuning.CurrentHealth <= 0 && this.gameObject.tag == "PathFinder")
            {
                BattlefieldOverwatch.Observer.DestroyedEnemyThree = true;
                Destroy(this.gameObject);
            }

            // yeah, uh...redfox too...
            if (BotTuning.CurrentHealth <= 0 && this.gameObject.tag == "RedFox")
            {
                BattlefieldOverwatch.Observer.DestroyedEnemyFour = true;
                Destroy(this.gameObject);
            }
        }
    }

    void FollowWaypointsPF(Transform PathPoint) {
        if (BotEngine.GoThatWay(PathPoint.position, BotTuning.TankTurnValue))
        {
            //do nothing
        }
        else {
            BotEngine.MoveTank(BotTuning.TankSpeedValue);
        }

        if (Vector3.SqrMagnitude(PathPoint.position - TankOrigin.position) < (RelativeArea * RelativeArea)) {
            if (MyTwenty < MainPath.Length - 1)
            {
                MyTwenty++;
            }
            else {
                MyTwenty = 0;
            }
        }
    }

    void FollowWaypointsG(Transform PathPoint)
    {
        if (BotEngine.GoThatWay(PathPoint.position, BotTuning.TankTurnValue))
        {
            //do nothing
        }
        else
        {
            BotEngine.MoveTank(BotTuning.TankSpeedValue);
        }

        if (Vector3.SqrMagnitude(PathPoint.position - TankOrigin.position) < (RelativeArea * RelativeArea))
        {
            if (MyTwenty < GuardianPath.Length - 1)
            {
                MyTwenty++;
            }
            else
            {
                MyTwenty = 0;
            }
        }
    }

    void FollowWaypointsBR(Transform PathPoint)
    {
        if (BotEngine.GoThatWay(PathPoint.position, BotTuning.TankTurnValue))
        {
            //do nothing
        }
        else
        {
            BotEngine.MoveTank(BotTuning.TankSpeedValue);
        }

        if (Vector3.SqrMagnitude(PathPoint.position - TankOrigin.position) < (RelativeArea * RelativeArea))
        {
            if (MyTwenty < HoundFoxPath.Length - 1)
            {
                MyTwenty++;
            }
            else
            {
                MyTwenty = 0;
            }
        }
    }

    void PathfinderBehavior() {
        if (!CanHearPlayer)
        {
            FollowWaypointsPF(MainPath[MyTwenty]);
        }
        else {
            ShootTheThing();
            if (BotEngine.CanAIFire == false && Time.time > BotCannonReady) {
                BotEngine.CanAIFire = true;
            }
        }
    }

    void GuardianBehavior() {
        if (!CanHearPlayer)
        {
            FollowWaypointsG(GuardianPath[MyTwenty]);
        }
        else {
            ShootTheThing();
            if (BotEngine.CanAIFire == false && Time.time > BotCannonReady) {
                BotEngine.CanAIFire = true;
            }
        }
    }

    void BloodhoundBehavior() {
        if (!CanHearPlayer)
        {
            FollowWaypointsBR(HoundFoxPath[MyTwenty]);
        }
        else {
            BotEngine.GoThatWay(PlayerLocation.position, BotTuning.TankTurnValue);
            if (Vector3.SqrMagnitude(PlayerLocation.position - TankOrigin.position) > 2) {
                BotEngine.MoveTank(BotTuning.TankSpeedValue);
            } else
            {
                BotEngine.MoveTank(0f);
            }
        }
    }

    void RedFoxBehavior() {
        if (!CanHearPlayer)
        {
            FollowWaypointsBR(HoundFoxPath[MyTwenty]);
        }
        else {
            CreateRetreat();

            BotEngine.GoThatWay(RetreatVector, BotTuning.TankTurnValue);
            BotEngine.MoveTank(BotTuning.TankSpeedValue);
        }
    }

    void GuardsmenRoutine() {
        if (Vector3.SqrMagnitude(PlayerLocation.position - TankOrigin.position) < SoundRange)
        {
            CanHearPlayer = true;
        }
        else {
            CanHearPlayer = false;
        }
    }

    void CreateRetreat() {
        RetreatVector = (-1 * (PlayerLocation.position - TankOrigin.position));
        RetreatVector.Normalize();
        RetreatVector *= 1;
    }

    void ShootTheThing() {
        BotEngine.GoThatWay(PlayerLocation.position, BotTuning.TankTurnValue);
        RaycastHit TankTarget;
        if (Physics.Raycast(this.transform.position, this.transform.forward, out TankTarget, 10))
        {
            if (TankTarget.collider.tag == "Player")
            {
                if (BotEngine.CanAIFire == true)
                {
                    // Uses the generic fire function
                    BotEngine.FireCannon();

                    // Reloading the robot's cannon
                    BotCannonReady = Time.time + BotTuning.Reload;

                    // The robot can't fire until this switch is flipped
                    BotEngine.CanAIFire = false;
                }
            }
        }
        
    }

    
}
