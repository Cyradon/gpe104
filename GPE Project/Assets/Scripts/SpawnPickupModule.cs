﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPickupModule : MonoBehaviour {

    public GameObject[] PickupChoice;
    public float SpawnTime = 30;
    private float SpawnInterval;
    private Transform ThisPos;
    public GameObject ExistingPickup;

	// Use this for initialization
	void Start () {
        ThisPos = gameObject.GetComponent<Transform>();
        SpawnInterval = SpawnTime + Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (ExistingPickup == null) {
            if (Time.time > SpawnInterval) {
                ExistingPickup = Instantiate(PickupChoice[UnityEngine.Random.Range(0, PickupChoice.Length)], ThisPos.position, Quaternion.identity) as GameObject;
                SpawnInterval = SpawnTime + Time.time;
            }
        } else
        {

            SpawnInterval = SpawnTime + Time.time;
        }
	}
}
