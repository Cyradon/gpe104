﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileModule : MonoBehaviour {

    // How long the shell actually gets to fly. is editable
    public float ShellLifeTime = 0.5f;

    // The actual storage for the timer
    private float ExpirationTime;

	// Use this for initialization
	void Start () {
        ExpirationTime = Time.time + ShellLifeTime;
	}
	
	// Update is called once per frame
	void Update () {
        ShellTimer();
	}

    // when the shell impacts something...
    void OnCollisionEnter(Collision ShellBump)
    {
        // make sure it's something it can be destroyed on...
        if (ShellBump.gameObject.tag == "Bloodhound" || ShellBump.gameObject.tag == "Obstacle" || ShellBump.gameObject.tag == "Player" || ShellBump.gameObject.tag == "Guardian" || ShellBump.gameObject.tag == "PathFinder" || ShellBump.gameObject.tag == "RedFox") {
            // then destroy it
            Destroy(this.gameObject);
        }
    }

    // The shell doesn't last long. This makes sure it's not a fixture
    void ShellTimer() {
        // Checking the timer
        if (Time.time > ExpirationTime) {
            // eliminating the fixture
            Destroy(this.gameObject);
        }
    }
}
