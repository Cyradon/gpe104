﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EventsModule : MonoBehaviour {

    public Toggle RandomToggle;
    
    public static bool IsRandomLoad;

    public Toggle DayMapToggle;

    public static bool IsMapOfDay;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadLevelMap() {
        if (IsRandomLoad == true || IsMapOfDay == true)
        {
            SceneManager.LoadScene("Main");
        }
    }

    public void OptionsOpen() {
        SceneManager.LoadScene("Options");
    }

    public void ExitGame() {
        Application.Quit();
    }

    public void SelectRandom()
    {
        if (RandomToggle.isOn)
        {
            IsRandomLoad = true;
            IsMapOfDay = false;
            DayMapToggle.isOn = false;
        }
    }

    public void SelectMapOfDay() {
        if (DayMapToggle.isOn)
        {
            IsMapOfDay = true;
            IsRandomLoad = false;
            RandomToggle.isOn = false;
        }
    }
}
