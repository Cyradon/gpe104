﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlefieldOverwatch : MonoBehaviour {

    // did the player destroy the first enemy tank
    public bool DestroyedEnemyOne = false;

    // did the player destroy the second enemy tank
    public bool DestroyedEnemyTwo = false;

    // did the player destroy the third enemy tank
    public bool DestroyedEnemyThree = false;

    // did the player destroy the fourth enemy tank
    public bool DestroyedEnemyFour = false;

    

    // Observer because who doesn't love watching a good battle
    public static BattlefieldOverwatch Observer;

    void Awake()
    {
        // making sure the system hasn't accidentally called in more than one Observer
        if (Observer == null)
        {
            Observer = this;
        }
        // and if it has, then the second one will be dealt with.
        else
        {
            Debug.LogError("Unauthorized Observer Detected. Only one Observer may oversee operations at any one time.");
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start() {
        DestroyedEnemyOne = false;
        DestroyedEnemyTwo = false;
        DestroyedEnemyThree = false;
        DestroyedEnemyFour = false;
	}
	
	// Update is called once per frame
	void Update () {
        PlayerScoring();
	}

    // The player earns a score by destroying tanks, which is registered with this
    public void PlayerScoring() {
        if (DestroyedEnemyOne) {
            ScoringModule.TankScore.CurrentCount = ScoringModule.TankScore.CurrentCount + 100;
            DestroyedEnemyOne = false;
        }

        if (DestroyedEnemyTwo) {
            ScoringModule.TankScore.CurrentCount = ScoringModule.TankScore.CurrentCount + 200;
            DestroyedEnemyTwo = false;
        }

        if (DestroyedEnemyThree) {
            ScoringModule.TankScore.CurrentCount = ScoringModule.TankScore.CurrentCount + 150;
            DestroyedEnemyThree = false;
        }

        if (DestroyedEnemyFour)
        {
            ScoringModule.TankScore.CurrentCount = ScoringModule.TankScore.CurrentCount + 300;
            DestroyedEnemyFour = false;
        }
    }
}
