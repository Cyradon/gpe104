﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransmissionModule : MonoBehaviour {

    // three control schemes to look at
    public enum controlLayouts {WASD, Arrows};

    // sets the default for players
    public controlLayouts TControls = controlLayouts.WASD;

    // Can't have a transmission without an engine
    public EngineModule TEngine;

    // This tunes the engine to standard military regulations
    public TuningModule TuningData;

    public ScoringModule ScoreKeeper;

    // This determines the time it takes to reload for another shot
    private float CannonReadyTime;

    public bool isDestroyed = false;

    // Use this for initialization
    void Start () {
        // starting off the timer to reload for the AI
        CannonReadyTime = Time.time + TuningData.Reload;

        // Sets the initial hull integrity of tank
        TuningData.CurrentHealth = TuningData.HealthCount;

        // The AI gets to shoot first. Please save questions for later
        TEngine.CanAIFire = true;

        if (this.gameObject.tag == "Player")
        {
            this.ScoreKeeper.CurrentCount = 0;
        }

        
    }
	
	// Update is called once per frame
	void Update () {
        PlayerMovement();
	}

    // Base movement method established for both keyboard and AI movement control
    void PlayerMovement() {
        switch (TControls)
        {
            case controlLayouts.WASD:
                if (Input.GetKey(KeyCode.W))
                {
                    TEngine.MoveTank(TuningData.TankSpeedValue);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    TEngine.MoveTank(-TuningData.TankBackingValue);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    TEngine.TurnTank(-TuningData.TankTurnValue);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    TEngine.TurnTank(TuningData.TankTurnValue);
                }
                if (Input.GetKey(KeyCode.Space) && Time.time > CannonReadyTime)
                {
                    TEngine.FireCannon();
                    CannonReadyTime = Time.time + TuningData.Reload;
                }
                break;
            case controlLayouts.Arrows:
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    TEngine.MoveTank(TuningData.TankSpeedValue);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    TEngine.MoveTank(-TuningData.TankBackingValue);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    TEngine.TurnTank(-TuningData.TankTurnValue);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    TEngine.TurnTank(TuningData.TankTurnValue);
                }
                if (Input.GetKey(KeyCode.Space) && Time.time > CannonReadyTime)
                {
                    TEngine.FireCannon();
                    CannonReadyTime = Time.time + TuningData.Reload;
                }
                break;
        }
    }

    // The AI will use this to launch shells at you
    public void AIFire()
    {
        // first we need to make sure it can fire
        if (TEngine.CanAIFire == true)
        {
            // Uses the generic fire function
            TEngine.FireCannon();

            // Reloading the robot's cannon
            CannonReadyTime = Time.time + TuningData.Reload;

            // The robot can't fire until this switch is flipped
            TEngine.CanAIFire = false;
        }

        
    }

    // This is how tanks are destroyed and health is calculated
    public void OnCollisionEnter(Collision TankBody)
    {

        // Check for being hit by a shell at all, be it enemy or player
        if (TankBody.gameObject.tag == "EnemyShell" || TankBody.gameObject.tag == "PlayerShell") {
            // reduce health on hit
            TuningData.CurrentHealth--;

            // If the tank that was attacked was the bloodhound tank
            if (TuningData.CurrentHealth <= 0 && this.gameObject.tag == "Bloodhound")
            {
                // let the observer know that the bloodhound was destroyed
                BattlefieldOverwatch.Observer.DestroyedEnemyOne = true;
                // get on with actually removing it from the game
                Destroy(this.gameObject);
            }

            // Same deal with guardian as above
            if (TuningData.CurrentHealth <= 0 && this.gameObject.tag == "Guardian")
            {
                BattlefieldOverwatch.Observer.DestroyedEnemyTwo = true;
                Destroy(this.gameObject);
            }

            // ditto with pathfinder
            if (TuningData.CurrentHealth <= 0 && this.gameObject.tag == "PathFinder")
            {
                BattlefieldOverwatch.Observer.DestroyedEnemyThree = true;
                Destroy(this.gameObject);
            }

            // yeah, uh...redfox too...
            if (TuningData.CurrentHealth <= 0 && this.gameObject.tag == "RedFox")
            {
                BattlefieldOverwatch.Observer.DestroyedEnemyFour = true;
                Destroy(this.gameObject);
            }
        }
    }

}
