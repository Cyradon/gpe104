﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PickupModule {
    public PickupModule LootThing;

    public float UpgradeStayTime;

    public float LowSpeedBooster;
    public float HighSpeedBooster;

    public int RegeneratorLite;
    public int RegeneratorHeavy;

    public float RapidFireAmmo;

    public bool isHardUpgrade;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnObtain(TuningModule Recipient) {
        Recipient.TankSpeedValue += LowSpeedBooster;
        Recipient.TankSpeedValue += HighSpeedBooster;
        if (Recipient.CurrentHealth < Recipient.HealthCount)
        {
            Recipient.CurrentHealth += RegeneratorLite;
            Recipient.CurrentHealth += RegeneratorHeavy;
        }
        Recipient.Reload -= RapidFireAmmo;
    }

    public void OnRunOut(TuningModule Recipient) {
        Recipient.TankSpeedValue -= LowSpeedBooster;
        Recipient.TankSpeedValue -= HighSpeedBooster;
        Recipient.CurrentHealth -= RegeneratorLite;
        Recipient.CurrentHealth -= RegeneratorHeavy;
        Recipient.Reload += RapidFireAmmo;
    }
}
