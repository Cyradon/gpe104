﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupModule : MonoBehaviour {

    public List<PickupModule> PickUp;

    public TuningModule TankTuning;

	// Use this for initialization
	void Start () {
        PickUp = new List<PickupModule>();
	}
	
	// Update is called once per frame
	void Update () {
        List<PickupModule> RunOutUpgrade = new List<PickupModule>();

        foreach (PickupModule Upgrade in PickUp) {
            Upgrade.UpgradeStayTime -= Time.deltaTime;

            if (Upgrade.UpgradeStayTime <= 0) {
                RunOutUpgrade.Add(Upgrade);
            }
        }

        foreach (PickupModule Upgrade in RunOutUpgrade) {
            Upgrade.OnRunOut(TankTuning);
            PickUp.Remove(Upgrade);
        }

        RunOutUpgrade.Clear();
	}

    public void IncreasePower(PickupModule Upgrade) {
        Upgrade.OnObtain(TankTuning);

        if (!Upgrade.isHardUpgrade)
        {
            PickUp.Add(Upgrade);
        }
    }
}
